var AWS = require("aws-sdk");
var simpleParser = require("mailparser").simpleParser;

var s3 = new AWS.S3();
var documentClient = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event, context, callback) => {
    const mail = event.Records[0].ses.mail;

    console.log("Mail");
    console.log(JSON.stringify(mail));

    var getParams = {
        Bucket: "disposible-mails-bucket",
        Key: "awesome_crypto_me/" + mail.messageId,
    };
    let emailFromS3 = await s3.getObject(getParams).promise();
    let parsedEmail = await simpleParser(emailFromS3.Body);

    console.log("parsedEmail Subject: "  + parsedEmail.subject);
    console.log("parsedEmail Body: " + parsedEmail.textAsHtml);

    const sendToValues = parsedEmail.to.value;
    for (var ind = 0; ind < sendToValues.length; ind++) {
        var elem = sendToValues[ind];
        var putItemMailParams = {
            Item: {
                "sendTo": elem.address,
                "messageId": mail.messageId
            },
            TableName: "DisposableEmails"
        };

        try {
            await documentClient.put(putItemMailParams).promise();
        } catch (err) {
            console.log(err);
        }
    }
    callback(null);
};
